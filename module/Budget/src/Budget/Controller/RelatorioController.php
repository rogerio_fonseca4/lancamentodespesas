<?php
namespace Budget\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

/**
 * RelatorioController
 *
 * @author
 *
 * @version
 *
 */

class RelatorioController extends AbstractActionController
{

    public function indexAction()
    {
        return new ViewModel(array());
    }
    
    
    public function consultarAction()
    {
        $meses = $this->params()->fromRoute("mes",0);
        $anos = $this->params()->fromRoute("ano",0);
       
        $gastoTable = $this->sm->get('Budget\Model\GastoTable');
        
        return new JsonModel(array(
            'retorno' => $gastoTable->getSomatorioGastoPorCategoria($meses,$anos),
            'success'=>true,
        ));
    }
    
    public function consultarDetalhesCategoriaPorMesAction()
    {
        $meses = $this->params()->fromRoute("mes",0);
        $categoria = $this->params()->fromRoute("ano",0);
          
        $gastoTable = $this->sm->get('Budget\Model\GastoTable');
        
        return new JsonModel(array(
            'retorno' => $gastoTable->getDetalheGastoPorCategoriaNoMes($meses,$categoria),
            'success'=>true,
        ));
    }
}
