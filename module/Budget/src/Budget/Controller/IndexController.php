<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Budget for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Budget\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Budget\Model\Categoria;
use Zend\EventManager\EventManagerInterface;
use Tweet\Service\TweetService;

class IndexController extends AbstractActionController
{

    private $categoriaTable;
    
    /**
     * Lista as categorias
     * 
     * @see \Zend\Mvc\Controller\AbstractActionController::indexAction()
     */
    public function indexAction()
    {
        return new ViewModel(array(
            'categorias' => $this->getCategoriaTable()->fetchAll()
        ));
    }

    /**
     * Cria uma nova categoria
     * 
     * @return \Zend\Http\Response|multitype:\Budget\Form\Categoria
     */
    public function addAction()
    {
        $form = new \Budget\Form\Categoria();
        $form->get('submit')->setValue("Salvar Categoria");
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $categoria = new Categoria();
            $form->setInputFilter($categoria->getInputFilter());
            $form->setData($request->getPost());
            
            if ($form->isValid()) {
                $categoria->exchangeArray($form->getData());
                
                if ($this->sm->get('Budget\Model\CategoriaTable')->save($categoria)) {
                    $this->flashMessenger()->addMessage(array(
                        "tipoMensagem" => "alert-success",
                        "mensagens" => 'Cadastrado com Sucesso'
                    ));
                    return $this->redirect()->toRoute("budget", array(
                        'action' => 'add'
                    ));
                }
            } else {
                $this->flashMessenger()->addMessage(array(
                                                        "tipoMensagem" => "alert-danger",
                                                        "mensagens" => $form->getInputFilter()
                                                            ->getMessages()
                ));
                return $this->redirect()->toRoute("budget", array(
                                                                    'action' => 'add'
                                                                ));
            }
        }
        
        return array(
            "form" => $form
        );
    }

    /**
     * Cria uma nova categoria
     * 
     * @return \Zend\Http\Response|multitype:\Budget\Form\Categoria
     */
    public function editAction()
    {
        $id = $this->params()->fromRoute("idgasto",0);
        if (! $id) {
            return $this->redirect()->toRoute("budget", array(
                "action" => "index"
            ));
        }
        
        try {
            $categoria = new Categoria();
            $categoria->idcategoria = $id;
            $categoria = $this->getCategoriaTable()->get($categoria);
        } catch (\Exception $e) {
            $this->flashMessenger()->addMessage(array("tipoMensagem" => "alert-danger",
                                                      "mensagens" => "Nome da categoria obrigatório")
                                               );
            return $this->redirect()->toRoute("budget", array("controller" => "categoria", "action" => "edit"
                                             ));
        }
        
        $form = new \Budget\Form\Categoria();
        $form->bind($categoria);
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            $form->get('submit')->setValue("Alterar Categoria");
            $form->setInputFilter($categoria->getInputFilter());
            $form->setData($request->getPost());
            
            if ($form->isValid()) {
                $this->getCategoriaTable()->save($categoria);
                $this->flashMessenger()->addMessage(array("tipoMensagem" => "alert-success", 
                                                          "mensagens" =>'Cadastrado com Sucesso'));
                return $this->redirect()->toRoute("budget",array("controller"=>"categoria"));
            } else {
                $this->flashMessenger()->addMessage(array("tipoMensagem" => "alert-danger",
                                                          "mensagens" => $form->getInputFilter()->getMessages()));
                return $this->redirect()->toRoute("budget",array('controller'=>'Categoria', "action" => "edit"));
            }
        }
        
        return array(
            "form" => $form,
            "idcategoria" => $categoria->idcategoria
        );
    }

    /**
     */
    public function deleteAction()
    {
        
    }

    /**
     * Recupera o servico de categoria Table
     * 
     * @return \Budget\Model\CategoriaTable
     */
    public function getCategoriaTable()
    {
        if (! $this->categoriaTable) {
            $this->categoriaTable = $this->sm->get('Budget\Model\CategoriaTable');
        }
        return $this->categoriaTable;
    }
}
