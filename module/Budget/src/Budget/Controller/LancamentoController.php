<?php
namespace Budget\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * LancamentoController
 *
 * @author
 *
 * @version
 *
 */
use Budget\Form\GastoForm;
use Budget\Form\GastoFilter;
use Zend\Http\Request;
use Budget\Model\Gasto;

class LancamentoController extends AbstractActionController
{

    private $table;

    /**
     * Variável é criada no eventManager na trigger de dispatch
     * @var Zend\ServiceManager\ServiceManager
     */
    public $sm;
    
    /**
     * Lista as categorias
     * @return \Zend\View\Model\ViewModel
     */
    public function listAction()
    {
        $gastoTable = $this->sm->get('Budget\Model\GastoTable');
        return new ViewModel(array(
            'lancamentos' => $gastoTable->getlistaGastosCategoria()
        ));
    }
    
    /**
     * The default 
     */
    public function indexAction()
    {
        $form = new GastoForm("form",$this->sm->get('Budget\Model\CategoriaTable'));
        
        return new ViewModel(array(
            "form" => $form,
            "title" => "Realizar Lançamento"
        ));
    }

    /**
     * 
     * @param Request $request
     * @return \Budget\Form\GastoForm
     */
    private function getForm($request)
    {
        $form = new GastoForm("form",$this->sm->get('Budget\Model\CategoriaTable'));
        $form->setInputFilter(GastoFilter::getSingleton()->getInputFilter())
             ->setData($request->getPost());
        return $form;
    }
    
    /**
     * Adicionar a despesa no banco de dados
     */
    public function adicionarAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form = $this->getForm($request);
            if ($form->isValid()) {
                $gasto = new \Budget\Model\Gasto();
                $gasto->exchangeArray($form->getData());
                if($this->sm->get('Budget\Model\GastoTable')->save($gasto)){
                    $this->flashMessenger()->addMessage(array("tipoMensagem" => "alert-success", 
                                                              "mensagens" =>'Cadastrado com Sucesso'));
                    return $this->redirect()->toRoute("lancamento",
                                                      array('action'=>'list'));
                }
            } else {
                $this->flashMessenger()->addMessage(array("tipoMensagem" => "alert-danger",
                                                          "mensagens" => $form->getInputFilter()->getMessages()));
                return $this->redirect()->toRoute("lancamento",array('action'=>'index'));
            }
        }
        return array(
            "form" => $form,
            "title" => "Realizar Lançamento"
        );
    }
    
    /**
     * Cria uma nova categoria
     *
     * @return \Zend\Http\Response|multitype:\Budget\Form\Categoria
     */
    public function editAction()
    {
        $id = $this->params()->fromRoute("idgasto", 0);
        if (! $id) {
            return $this->redirect()->toRoute("lancamento", array(
                                               "action" => "index"
            ));
        }
    
        try {
            $gasto = new Gasto();
    
            $gasto->idgasto = $id;
            $gastoTable = $this->sm->get('Budget\Model\GastoTable');
            $gasto = $gastoTable->get($gasto);
    
        } catch (\Exception $e) {
            $this->flashMessenger()->addMessage(array("tipoMensagem" => "alert-danger",
                                                        "mensagens" => "Código do Gasto obrigatorio")
            );
            return $this->redirect()->toRoute("lancamento", array("action" => "edit"
                                             ));
        }
    
        $form = $this->getForm($this->getRequest());
        $form->bind($gasto);
        $request = $this->getRequest();
        
        $form->setAttribute('action', $this->url('lancamento', array('action' => 'edit')));
        
        $viewModel = new ViewModel(array("form" => $form,
                                         "idgasto" => $gasto->idgasto,
                                         "title" => "Alterar Lançamento"
                                   ));
        $viewModel->setTemplate('budget/lancamento/index.phtml');
        return $viewModel;
    }
    
    public function getSql()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        return $sql->select();
    }

}