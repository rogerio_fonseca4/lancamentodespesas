<?php
namespace Budget\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Mvc\Controller\Plugin\FlashMessenger as FlashMessenger;

class MensagensFlash extends AbstractHelper
{
    protected $flashMessenger;
    
    public function __construct(FlashMessenger $flashMessenger)
    {
        $this->flashMessenger = $flashMessenger;
    }   

    public function __invoke()
    {
        $mensagem = "";
        if ($this->flashMessenger->hasMessages()) {
            $messages = $this->flashMessenger->getMessages();
        
            $mensagem = "<div class='alert ". $messages[0]["tipoMensagem"] . "'>";
            $mensagem .="<button type='button' class='close' data-dismiss='alert'>&times;</button>";
        
            $messages = $messages[0]["mensagens"];
            
            if (is_array($messages)) {
                foreach ($messages as $key => $value) {
                    if (is_array($value)) {
                        foreach ($value as $key2 => $value2) {
                            $mensagem .= $key . " " . $value2 . "<br />";
                        }
                    }
                }
            }else
            {
                $mensagem .= $messages;
            }
        
            $mensagem .= '</div>';
        }
        
        return $mensagem;
    }
    
}