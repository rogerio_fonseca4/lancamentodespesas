<?php
namespace Budget\Form;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\Validator;
/**
 * 
 * @author rogerio
 *
 */
class GastoFilter implements InputFilterAwareInterface
{
    private $inputFilter;
    
    private static $instance;
    
    /**
     * return Budget\Form\GastoFilter
     */
    public static function getSingleton()
    {
        if (!isset(self::$instance)) {
            $class = __CLASS__;
            self::$instance = new $class;
        }
        
        return self::$instance;
    }
    
    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $this->inputFilter = new InputFilter();
            $this->inputFilter->add(array(
                'name' => 'vlgasto',
                'required' => true,
            ));
            $this->inputFilter->add(array(
                'name' => 'dsgasto',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'not_empty',
                    ),
                    array(
                        'name' => 'string_length',
                        'options' => array(
                            'min' => 2
                        ),
                    ),
            )));
        }
        return $this->inputFilter;
    }
    
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }
    
}
