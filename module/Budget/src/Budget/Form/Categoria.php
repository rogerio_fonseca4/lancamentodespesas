<?php
namespace Budget\Form;

use Zend\Form\Form;

class Categoria extends Form
{
    public function __construct($name = null)
    {
        parent::__construct("categoria");
        $this->add(array(
            "name" => "nocategoria",
            "type" => "text",
            "id" => "nocategoria",
            "options" => array(
                    "label" => "Nome da Categoria",
            ), 
        ));
        
        $this->add(array(
            "name" => "idcategoria",
            "type" => "hidden",
        ));
        
        $this->add(array(
            "name" => "submit",
            "type" => "submit",
            "attributes" => array(
                "value" => "Salvar",
                "id" => "salvar" 
                ),
        ));
    }
    
}