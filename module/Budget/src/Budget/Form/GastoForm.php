<?php
namespace Budget\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Budget\Model\CategoriaTable;
class GastoForm extends Form
{
    private $categoriaTable;

    public function __construct($name = null,CategoriaTable $categoriaTable = null)
    {
        $this->categoriaTable = $categoriaTable;
        
        parent::__construct("categoria");
        
        $this->add(array(
            "name" => "idgasto",
            "type" => "hidden"
        ));
        
        $this->add(array(
            "name" => "dtgasto",
            "type" => "text",
            "id" => "dtgasto",
            "options" => array(
                "label" => "Data"
            )
        ));
        
        $this->add(array(
            "name" => "vlgasto",
            "type" => "text",
            "id" => "vlgasto",
            "options" => array(
                "label" => "Valor",
            ),
            'attributes' => array(
               "class" =>"form-control",
               "placeholder" => "Valor",
            ),
        ));
        
        $this->add(array(
            "name" => "dsgasto",
            "type" => "text",
            "id" => "dsgasto",
            "options" => array(
                "label" => "Descrição"
            )
        ));
        
        $this->add(array(
            "name" => "idcategoria",
            "type" => "select",
            "id" => "idcategoria",
            "options" => array(
                "label" => "Categoria",
                'value_options' => $this->getOptionsForSelect(),
                ),
            )
        );
        
        $this->add(array(
            "name" => "strecorrente",
            "type" => "checkbox",
            "id" => "strecorrente",
            'options' => array(
                'label' => 'Recorrente',
                'use_hidden_element' => true,
                'checked_value' => '1',
                'unchecked_value' => '0'
            )
        ));
        
        $this->add($this->getListaCategorias());
        
        $this->add(array(
            "name" => "submit",
            "type" => "submit",
            "attributes" => array(
                "value" => "Salvar Lançamento",
                "id" => "salvar"
            )
        ));
    }

    public function getListaCategorias()
    {
        $select = new Element("select");
        $select->setLabel('Categorias');
        $select->setValue(array(
            '0' => 'French',
            '1' => 'English',
            '2' => 'Japanese',
            '3' => 'Chinese'
        ));
        return $select;
    }
    
    /**
     * 
     * @return multitype:NULL
     */
    public function getOptionsForSelect()
    {
        $selectData = array();
        if($this->categoriaTable){
            $data  = $this->categoriaTable->fetchAll();
            
            foreach ($data as $selectOption) {
                $selectData[$selectOption["idcategoria"]] = $selectOption["nocategoria"];
            }
        }
        return $selectData;
    }
}