<?php
namespace Budget\Model;

/**
 * @author rogerio
 * @primary idgasto
 */

class Gasto{
    public $idgasto;
    public $dtgasto;
    public $dsgasto;
    public $vlgasto;
    public $idcategoria;
    public $strecorrente;
    
    public function exchangeArray($data)
    {
        $this->idgasto = (!empty($data['idgasto'])) ? $data['idgasto'] : null;
        $this->dtgasto = (!empty($data['dtgasto'])) ? $data['dtgasto'] : null;
        $this->dsgasto = (!empty($data['dsgasto'])) ? $data['dsgasto'] : null;
        $this->vlgasto = (!empty($data['vlgasto'])) ? $data['vlgasto'] : null;
        $this->idcategoria = (!empty($data['idcategoria'])) ? $data['idcategoria'] : null;
        $this->strecorrente = (!empty($data['strecorrente'])) ? $data['strecorrente'] : null;
    }
    
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}