<?php
namespace Budget\Model;

use Zend\Db\TableGateway\TableGateway;
use Budget\Model\Db\TableGatewayCorporativo;
use Zend\Db\Sql\Where;
use Zend\Db\Sql\Predicate\Expression;
use Zend\Db\Sql\Predicate\Predicate;

class GastoTable extends TableGatewayCorporativo
{

    protected $tableGateway;
    protected $cached = true;
        
    public function __construct(TableGateway $tableGateway, $cache)
    {
        parent::__construct($tableGateway,$cache);
    }

    /**
     * Retorna a lista de gastos com a categoria relacionada
     * @return \Zend\Cache\Storage\Adapter\mixed
     */
    public function getlistaGastosCategoria()
    {
        $queryString =  $this->sql->select()->from(array('g' => 'tbgasto'))
                                            ->join(array('c' => 'tbcategoria'),'g.idcategoria = c.idcategoria')
                                            ->order('dtgasto DESC');
        return $this->execSqlQuery($queryString, __FUNCTION__);
    }

    /**
     * 
     * @return array
     */
    public function getSomatorioGastoPorCategoria($meses,$anos)
    {
        $exp = new \Zend\Db\Sql\Expression('MONTH("dtgasto")');
        $queryString =  $this->sql->select()->columns(array("vlgasto" => new \Zend\Db\Sql\Expression('SUM(vlgasto)'),
                                                            "mesGasto" => new \Zend\Db\Sql\Expression('MONTH(dtgasto)'),            
                                            ))
                                            ->from(array('g' => 'tbgasto'))
                                            ->join(array('c' => 'tbcategoria'),'g.idcategoria = c.idcategoria',array('nocategoria','idcategoria'))
                                            ->group(array("c.nocategoria","mesgasto",'idcategoria'))
                                            ->order('vlgasto DESC')
                                            ->having(array("mesGasto" => explode(",", $meses)));      
        return $this->execSqlQuery($queryString, __FUNCTION__);
    }
    
    /**
     *
     * @return array
     */
    public function getDetalheGastoPorCategoriaNoMes($mes,$idcategoria)
    {
        $exp = new \Zend\Db\Sql\Expression('MONTH("dtgasto")');
        $where = new Where();
        $where->addPredicate(new Expression('MONTH(dtgasto) = ?', $mes ))
              ->addPredicate(new Expression("idcategoria = ?", $idcategoria));
        $queryString =  $this->sql->select()->columns(array("dsgasto","vlgasto","idcategoria","dtgasto","mesGasto" => new \Zend\Db\Sql\Expression('MONTH(dtgasto)')))
                                            ->from(array('g' => 'tbgasto'))
                                            ->order('vlgasto DESC')
                                            ->where($where);
        return $this->execSqlQuery($queryString, __FUNCTION__);
    }
    

}