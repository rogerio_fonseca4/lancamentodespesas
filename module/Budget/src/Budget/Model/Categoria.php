<?php
namespace Budget\Model;
// Add these import statements
/**
 *
 * @primary idcategoria
 */
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;

	class Categoria
	{
	    
		public $idcategoria;
		public $nocategoria;
		public $idcategoriapai;
		protected $inputFilter;

		public function exchangeArray($data)
		{
			$this->idcategoria = (!empty($data['idcategoria'])) ? $data['idcategoria'] : null;
			$this->nocategoria = (!empty($data['nocategoria'])) ? $data['nocategoria'] : null;
			$this->idcategoriapai = (!empty($data['idcategoriapai'])) ? $data['idcategoriapai'] : null;
		}

		public function getArrayCopy()
		{
		  return get_object_vars($this);
		}
		    
		// Add content to these methods:
		public function setInputFilter(InputFilterInterface $inputFilter)
 		{
 			throw new \Exception("Not used");
 		}

 		public function getInputFilter()
 		{
 			if (!$this->inputFilter) {
 				$inputFilter = new InputFilter();
				$inputFilter->add(array(
						'name' => 'idcategoria',
						'required' => true,
						'filters' => array(
							array('name' => 'Int'),
						),
				));
			

			$inputFilter->add(array(
				'name' => 'nocategoria',
				'required' => true,
				'filters' => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
			));

			$this->inputFilter = $inputFilter;
		}

		return $this->inputFilter;
	}
}

