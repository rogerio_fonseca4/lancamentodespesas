<?php
namespace Budget\Model\Db;

use Budget\Module;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Di\ServiceLocator;
use Zend\Di\ServiceLocatorInterface;

class Tables extends AbstractActionController
{

    const TBGASTO = "tbgasto";
    const TBCATEGORIA = "tbcategoria";
    
    public static $listaModels = array(
        "CategoriaTable" => "Budget\Model\CategoriaTable",
        "GastoTable" => "Budget\Model\GastoTable"
    );

    /**
     *
     * @param unknown $table            
     * @param string modelTable
     *            Ex: Tables::listaModels["CategoriaTable"] ou Budget\Model\CategoriaTable
     * @return Table
     */
    public function getTable(&$table, $modelTable, $sm)
    {
        if (!$table) {
            $table = $sm->get($modelTable);
        }
        return $table;
    }
}