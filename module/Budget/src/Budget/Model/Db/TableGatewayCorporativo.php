<?php
namespace Budget\Model\Db;
use Zend\Db\TableGateway\TableGateway;
use Budget;
use Zend\Form\Annotation\Object;
use Zend\Cache\Storage\Adapter\ZendServerShm;
use Zend\Db\Sql\Sql;

class TableGatewayCorporativo
{
    /**
     * 
     * @var Zend\Db\TableGateway\TableGateway
     */
    protected $tableGateway;
    /**
     * @var ZendServerShm
     */
    protected $cache;
    /**
     * Código identificador da entidade
     * @var int 
     */
    private $idEntidade;
    
    protected $sql;
    
    public function __construct(TableGateway $tableGateway, $cache)
    {
        $this->tableGateway = $tableGateway;
        $this->cache = $cache;
        $this->setSql();
    }
    
    private function getCacheName()
    {
        return $this->tableGateway->getTable() . "-cache";
    }
    
    protected function setSql()
    {
        $this->sql = new Sql($this->tableGateway->getAdapter());
    }
    /**
     * recupera todos 
     * @return \Zend\Db\ResultSet\ResultSet
     */
    public function fetchAll()
    {
        if($this->isCached()){
            $key    = $this->getCacheName();
            $resultSet = $this->cache->getItem($key, $success);
            
            if (!$resultSet) {
                $resultSet = $this->tableGateway->select()->toArray();
                $this->cache->setItem($key, $resultSet);
            }
            return $resultSet;
        }
        
        return $this->tableGateway->select()->toArray();
    }
    
    
    
    /**
     * Recupera o objeto por Id
     * @param Budget\Model
     * @throws \Exception
     * @return Ambigous <multitype:, ArrayObject, NULL>
     */
    public function get($obj)
    {
        $id = $this->getIdEntidade($obj);
        $rowset = $this->tableGateway->select(array("$id" => $obj->$id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }
    
    /**
     * Salva a categoria
     * @param Budget\Model\
     * @throws \Exception
     */
    public function save($obj)
    {
        $data = $obj->getArrayCopy();
        unset($data["inputFilter"]);
        
        $id = $this->getIdEntidade($obj);
        if ($obj->$id == 0) {
            return $this->insertData($data);
        } else {
            if ($this->get($obj)) {
                if($this->isCached()){
                    $key    = $this->getCacheName();
                    $this->cache->flush();
                }
                return $this->tableGateway->update($data, array("$id" => $obj->$id));
            } else {
                throw new \Exception('Código identificador inexistente');
            }
        }
    }
    
    /**
     * Retorna o Código identificador da entidade
     * @param object $entidade
     * @return int
     */
    public function getIdEntidade($entidade)
    {
        if(!$this->idEntidade){
            $this->idEntidade = $this->getPrimaryKeyByDocComment($entidade);
        }   
        return $this->idEntidade;        
    }
    
    /**
     * 
     * @param unknown $entidade
     * @return string
     */
    private function getClassDocument($entidade)
    {
        $reflection = new \ReflectionClass($entidade);
        return $reflection->getDocComment();
    }
    /**
     * 
     * @param Object $entidade
     */
    public function getPrimaryKeyByDocComment($entidade)
    {
        $pattern = '/@primary \bid[a-z]+/';
        preg_match($pattern, $this->getClassDocument($entidade), $matches, PREG_OFFSET_CAPTURE, 3);
        $id = explode(" ", $matches[0][0]);
        return $id[1];
    }
    
    /**
     * 
     * @param array $data
     * @return boolean
     */
    public function insertData(array $data)
    {
        $insertData = $this->tableGateway->insert($data);
        if($this->isCached()){
            $key    = $this->getCacheName();
            $resultSet = $this->cache->flush();
        }   
        if($insertData){ 
            return $insertData;
        }    
    }
    
    
    /**
     * exlui a categoria
     * @param id $id
     */
    public function deleteCategoria($id)
    {
        $this->tableGateway->delete(array('id' => (int) $id));
    }
    
    /**
     * Valida se a tabela está com o atributo cached = true
     */
    public function isCached()
    {
        return $this->cached;
        
    }
    /**
     * 
     * @param unknown $select
     * @param unknown $parametros
     */
    public function getConsulta($key,$sql,$select,$parametros)
    {
        $query = $sql->prepareStatementForSqlObject($select);
        $result = $query->execute($parametros);
        $resultSet = \Zend\Stdlib\ArrayUtils::iteratorToArray($resultSet);
        return $resultSet;
    }
    
   /**
    * Executa a QueryString do tipo select() e retorna a consulta no formato de array e salva no cache
    * @param array $queryString
    */
    public function execSqlQuery($queryString, $key = "")
    {
        //$resultSet = $this->cache->getItem($key);
        //if(!$resultSet){
            $adapter = $this->tableGateway->getAdapter();
            $selectString = $this->sql->getSqlStringForSqlObject($queryString);
            $resultSet = $adapter->query($selectString, $adapter::QUERY_MODE_EXECUTE)->toArray();
            $this->cache->setItem($key,$resultSet);
        //}
        return $resultSet;
         
    }
    
}