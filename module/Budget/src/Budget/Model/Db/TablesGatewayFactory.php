<?php
namespace Relatorio\Model\Db;

use Zend\ServiceManager\ServiceManager;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Form\Annotation\Object;

class TablesGatewayFactory
{
    private $adapter;
    private $resultset;
    /**
     * @var Zend\ServiceManager\ServiceManager
     */
    private $sm;
    private $cache;
    
    const TB_EMPRESA = "empresas";
    const TB_ATIVIDADE = "atividade";

    public function __construct(ServiceManager $sm,ResultSet $resultset)
    {
        $this->adapter = $sm->get('Zend\Db\Adapter\Adapter');
        $this->cache = $sm->get("cache");
        $this->sm = $sm;
        $this->resultset = $resultset;    
    }
    
    /**
     * 
     * @return \Relatorio\Model\EmpresaTable
     */
    public function getEmpresaTable()
    { 
        $tableGateway = $this->getTableGateway(new Relatorio\Model\Empresa(),self::TB_EMPRESA);
        $table = new \Relatorio\Model\EmpresaTable($tableGateway,$this->cache);
        return $table;
    }
    
    /**
     *
     * @return \Relatorio\Model\EmpresaTable
     */
    public function getAtividadeTable()
    {
        $tableGateway = $this->getTableGateway(new \Relatorio\Model\Atividade(),self::TB_ATIVIDADE);
        $table = new \Relatorio\Model\AtividadesTable($tableGateway,$this->cache);
        return $table;
    }
    
    /**
     * 
     * @param Object $table ex: Relatorio\Model\Empresa 
     * @param string $nomeTabela Nome da tabela no banco de dados Ex: empresas
     * @return \Zend\Db\TableGateway\TableGateway
     */
    public function getTableGateway($table,$nomeTabela)
    {
        $resultSetPrototype = $this->resultset->setArrayObjectPrototype($table);
        return new TableGateway($nomeTabela, $this->adapter, null, $resultSetPrototype);
    }
}