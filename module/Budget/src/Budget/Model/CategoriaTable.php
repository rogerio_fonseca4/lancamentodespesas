<?php
namespace Budget\Model;

use Zend\Db\TableGateway\TableGateway;
use Budget\Model\Db\TableGatewayCorporativo;

class CategoriaTable extends TableGatewayCorporativo
{
	protected $tableGateway;
    protected $cached = true;
    
	public function __construct(TableGateway $tableGateway, $cache)
	{
	    parent::__construct($tableGateway,$cache);
	}
	
 }
