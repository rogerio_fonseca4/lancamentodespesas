<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Categoria' => 'Budget\Controller\IndexController',
            'Lancamento' => 'Budget\Controller\LancamentoController',
            'Relatorio' => 'Budget\Controller\RelatorioController',
        ),
    ),
    
    //ROTAS do módulo 
	'router' => array(
 		'routes' => array(
 		     'budget' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/[:controller/:action][/:idcategoria]',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Categoria',
                        'action' => 'index',
                    ),
                ),
 			 ),   
 		    
 		    'lancamento' => array(
 		        'type'    => 'segment',
 		        'options' => array(
 		            'route' => '/[:controller/:action][/:idgasto][/:mes][/:ano]',
					'constraints' => array(
	 					'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
	 					'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
	 				),
 		            'defaults' => array(
 		                'controller' => 'Lancamento',
 		                'action'     => 'index',
 		            ),
 		        ),
 		    ),
 		    
 		    'relatorio' => array(
 		        'type'    => 'segment',
 		        'options' => array(
 		            'route' => '/[relatorio/:action][/:mes][/:ano][/:idcategoria]',
 		            'constraints' => array(
 		                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
 		                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
 		            ),
 		            'defaults' => array(
 		                'controller' => 'Relatorio',
 		                'action'     => 'index',
 		            ),
 		        ),
 		    ),
 		),
 	),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
          'budget' =>  __DIR__ . '/../view',
        ),
        
    ),
    'service_manager' => array(),    
    
);
