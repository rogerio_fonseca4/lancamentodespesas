<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/Budget for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Budget;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Budget\Model\Categoria;
use Budget\Model\CategoriaTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Budget\Model\GastoTable;
use Budget\Model\Gasto;
use Budget\Model\Db\Tables;
use Budget\View\Helper\MensagensFlash;
use Zend\Mvc\MvcEvent;
use ZendLogLogger;
use Zend\ServiceManager\ServiceManager;

class Module implements AutoloaderProviderInterface
{

    public function onBootstrap(MvcEvent $event)
    {
       $eventManager       = $event->getApplication()->getEventManager();
       $sharedEventManager = $eventManager->getSharedManager();
       $sharedEventManager->attach('Zend\Mvc\Controller\AbstractActionController'
                                                ,MvcEvent::EVENT_DISPATCH,  array($this,"setUpControllerVariables"), 100);
       
       $sharedEventManager->attach('Zend\Mvc\Controller\AbstractActionController',MvcEvent::EVENT_DISPATCH_ERROR, function($event){
            $viewModel = $event->getViewModel();
            $viewModel->setTemplate('layout/error-layout');
       });

    }
    
    
    public function setUpControllerVariables(MvcEvent $e)
    {
        $controller = $e->getTarget();
        $controller->sm = $controller->getServiceLocator();
    }
    
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php'
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/', __NAMESPACE__)
                )
            )
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * 
     * @return multitype:multitype:NULL  |\Budget\View\Helper\MensagemFlash
     */
    public function getViewHelperConfig()
    {
        return array(
            'factories' => array(
                'mensagemFlash' => function ($sm)
                {
                      $flashMessage = $sm->getServiceLocator()->get('ControllerPluginManager')
                                                               ->get('flashmessenger');
                      return new MensagensFlash($flashMessage);
                },
        ),
        );
    }
    
    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Budget\Model\CategoriaTable' => function (ServiceManager $sm)
                {
                    $tableGateway = $sm->get('CategoriaTableGateway');
                    $cache = $sm->get("cache");
                    $table = new CategoriaTable($tableGateway,$cache);
                    return $table;
                },
                'CategoriaTableGateway' => function ($sm)
                {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Categoria());
                    return new TableGateway(Tables::TBCATEGORIA, $dbAdapter, null, $resultSetPrototype);
                },
                'Budget\Model\GastoTable' => function ($sm)
                {
                    $tableGateway = $sm->get('GastoTableGateway');
                    $cache = $sm->get("cache");
                    $table = new GastoTable($tableGateway,$cache);
                    return $table;
                },
                'GastoTableGateway' => function ($sm)
                {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Gasto());
                    return new TableGateway(Tables::TBGASTO, $dbAdapter, null, $resultSetPrototype);
                },
                'ZendLog' => function ($sm) {
                    $filename = 'log_' . date('F') . '.txt';
                    $log = new Logger();
                    $writer = new Stream('./data/logs/' . $filename);
                    $log->addWriter($writer);
                    
                    return $log;
                },
            ),
            'invokables' => array(
                "GetTables" => 'Budget\Models\GetTables',
            ),
        );
    }
}
