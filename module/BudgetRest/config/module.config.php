<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'BudgetRest\Controller\LancamentoRest' => 'BudgetRest\Controller\LancamentoRestController',
        ),
    ),
     // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'lancamento-rest' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/album-rest[/:id]',
                    'constraints' => array(
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'AlbumRest\Controller\LancamentoRest',
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array( //Add this config
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
);
